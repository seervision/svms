import type { MigrationAvailable, ReleaseTag } from "../resources";
import type { Sha256Sum } from "../types";
import type { MigrationApplied, MigrationAppliedAvailable } from "./migration";
import { semVerFromReleaseTag } from "./release-tag";
import {
  SemVer,
  hasEqualSemVerPrecedence,
  isSemVerSatifyingTarget,
  sortAscSemVer,
  sortDescSemVer,
} from "./sem-ver";

export enum MigItemKind {
  ___ = "Known", //                   no release Tag,  no Migration script, migration not Applied
  __A = "Applied", //                 no release Tag,  no Migration script, migration is Applied
  _M_ = "Migration", //               no release Tag, has Migration script, migration not Applied
  _MA = "MigrationApplied", //        no release Tag, has Migration script, migration is Applied
  T__ = "Tagged", //                 has release Tag,  no Migration script, migration not Applied
  T_A = "TaggedApplied", //          has release Tag,  no Migration script, migration is Applied
  TM_ = "TaggedMigration", //        has release Tag, has Migration script, migration not Applied
  TMA = "TaggedMigrationApplied", // has release Tag, has Migration script, migration is Applied
}

interface MigItemBase<TKind extends MigItemKind> {
  kind: TKind;
  semVer: SemVer;
}

interface MigItem___ extends MigItemBase<MigItemKind.___> {}
function createMigItem___(semVer: SemVer): MigItem___ {
  return {
    kind: MigItemKind.___,
    semVer,
  };
}

interface MigItem__A extends MigItemBase<MigItemKind.__A> {
  migration: MigrationApplied;
}
function createMigItem__A(
  semVer: SemVer,
  migration: MigrationApplied
): MigItem__A {
  return {
    kind: MigItemKind.__A,
    migration,
    semVer,
  };
}

interface MigItem_M_ extends MigItemBase<MigItemKind._M_> {
  migration: MigrationAvailable;
}
function createMigItem_M_(
  semVer: SemVer,
  migration: MigrationAvailable
): MigItem_M_ {
  return {
    kind: MigItemKind._M_,
    migration,
    semVer,
  };
}

interface MigItem_MA extends MigItemBase<MigItemKind._MA> {
  migration: MigrationAppliedAvailable;
}
function createMigItem_MA(
  semVer: SemVer,
  migration: MigrationAppliedAvailable
): MigItem_MA {
  return {
    kind: MigItemKind._MA,
    migration,
    semVer,
  };
}

interface MigItemT__ extends MigItemBase<MigItemKind.T__> {
  tag: ReleaseTag;
}
function createMigItemT__(semVer: SemVer, tag: ReleaseTag): MigItemT__ {
  return {
    kind: MigItemKind.T__,
    semVer,
    tag,
  };
}

interface MigItemT_A extends MigItemBase<MigItemKind.T_A> {
  migration: MigrationApplied;
  tag: ReleaseTag;
}
function createMigItemT_A(
  semVer: SemVer,
  tag: ReleaseTag,
  migration: MigrationApplied
): MigItemT_A {
  return {
    kind: MigItemKind.T_A,
    migration,
    semVer,
    tag,
  };
}

interface MigItemTM_ extends MigItemBase<MigItemKind.TM_> {
  migration: MigrationAvailable;
  tag: ReleaseTag;
}
function createMigItemTM_(
  semVer: SemVer,
  tag: ReleaseTag,
  migration: MigrationAvailable
): MigItemTM_ {
  return {
    kind: MigItemKind.TM_,
    migration,
    semVer,
    tag,
  };
}

interface MigItemTMA extends MigItemBase<MigItemKind.TMA> {
  migration: MigrationAppliedAvailable;
  tag: ReleaseTag;
}
function createMigItemTMA(
  semVer: SemVer,
  tag: ReleaseTag,
  migration: MigrationAppliedAvailable
): MigItemTMA {
  return {
    kind: MigItemKind.TMA,
    migration,
    semVer,
    tag,
  };
}

export type MigItemTagged = MigItemT__ | MigItemT_A | MigItemTM_ | MigItemTMA;
export type MigItemMigration =
  | MigItem_M_
  | MigItem_MA
  | MigItemTM_
  | MigItemTMA;
export type MigItemApplied = MigItem__A | MigItem_MA | MigItemT_A | MigItemTMA;

export type MigItem =
  | MigItem___
  | MigItemTagged
  | MigItemMigration
  | MigItemApplied;

export interface MigrationTarget {
  idx: number;
  val: Exclude<MigItem, MigItem___ | MigItemT__>;
}

/**
 * Reduce all resolved resources to known data structures (a.k.a. MigItems).
 *
 * @param semVersConfigured
 * @param releaseTags
 * @param migrationsApplied
 * @param migrationsAvailable
 * @returns
 */
export function migItemsFromResources(
  semVersConfigured: SemVer[],
  releaseTags: ReleaseTag[],
  migrationsApplied: MigrationApplied[],
  migrationsAvailable: MigrationAvailable[]
): MigItem[] {
  // separate all migrations
  const migrationsMA = migrationsApplied
    .map((valA) => {
      const migrationScript = migrationsAvailable.find((valB) =>
        isEqualMigration(valA, valB)
      );
      return !migrationScript
        ? null
        : {
            ...valA,
            ...migrationScript,
          };
    })
    .filter((val): val is MigrationAppliedAvailable => !!val);
  const migrations_A = migrationsApplied.filter(
    (valA) => !migrationsMA.some((valB) => isEqualMigration(valA, valB))
  );
  const migrationsM_ = migrationsAvailable.filter(
    (valA) => !migrationsMA.some((valB) => isEqualMigration(valA, valB))
  );

  // separate all migrations WITH tagged release
  const itemsTxx = releaseTags
    .map((releaseTag): MigItemTagged | null => {
      const semVer = semVerFromReleaseTag(releaseTag);
      if (!semVer) {
        return null;
      }

      // we ignore unconfigured (potentially older or newer) releases
      if (!semVersConfigured.some((val) => val.major === semVer.major)) {
        return null;
      }

      const isEqual = (val: { semVer: SemVer }) =>
        hasEqualSemVerPrecedence(semVer, val.semVer);

      const migrationTMA = migrationsMA.find(isEqual);
      if (migrationTMA) {
        return createMigItemTMA(semVer, releaseTag, migrationTMA);
      }

      const migrationT_A = migrations_A.find(isEqual);
      if (migrationT_A) {
        return createMigItemT_A(semVer, releaseTag, migrationT_A);
      }

      const migrationTM_ = migrationsM_.find(isEqual);
      if (migrationTM_) {
        return createMigItemTM_(semVer, releaseTag, migrationTM_);
      }

      return createMigItemT__(semVer, releaseTag);
    })
    .filter((val): val is MigItemTagged => !!val);

  // filter out all migrations WITHOUT a tagged release
  const items_xx = [...migrationsMA, ...migrations_A, ...migrationsM_]
    .filter(
      (valA) =>
        !itemsTxx.some((valB) =>
          isMigItemApplied(valB) || isMigItemAvailable(valB)
            ? isEqualMigration(valA, valB.migration)
            : hasEqualSemVerPrecedence(valA.semVer, valB.semVer)
        )
    )
    .map((valA) => {
      const isEqual = (valB: { semVer: SemVer; sha256sum: Sha256Sum }) =>
        isEqualMigration(valA, valB);

      const migration_MA = migrationsMA.find(isEqual);
      if (migration_MA) {
        return createMigItem_MA(valA.semVer, migration_MA);
      }

      const migration__A = migrations_A.find(isEqual);
      if (migration__A) {
        return createMigItem__A(valA.semVer, migration__A);
      }

      const migration_M_ = migrationsM_.find(isEqual);
      if (migration_M_) {
        return createMigItem_M_(valA.semVer, migration_M_);
      }

      // error solely there to satisfy TypeScript
      throw new Error("Cannot map migration: " + valA);
    });

  // all the known releases as a rest
  const items___ = semVersConfigured
    .filter((valA) => {
      const isEqual = (valB: { semVer: SemVer }) =>
        hasEqualSemVerPrecedence(valA, valB.semVer);

      return !itemsTxx.some(isEqual) && !items_xx.some(isEqual);
    })
    .map(createMigItem___);

  return [...itemsTxx, ...items_xx, ...items___].sort(orderForReverting);
}

/**
 *
 * @param migItem
 * @returns
 */
export function isMigItemApplied(migItem: MigItem): migItem is MigItemApplied {
  return [
    MigItemKind.__A,
    MigItemKind._MA,
    MigItemKind.T_A,
    MigItemKind.TMA,
  ].includes(migItem.kind);
}

/**
 *
 * @param migItem
 * @returns
 */
export function isMigItemAvailable(
  migItem: MigItem
): migItem is MigItemMigration {
  return [
    MigItemKind._M_,
    MigItemKind._MA,
    MigItemKind.TM_,
    MigItemKind.TMA,
  ].includes(migItem.kind);
}

/**
 *
 * @param migItem
 * @returns
 */
export function isMigItemMigration(
  migItem: MigItem
): migItem is MigItemApplied | MigItemMigration {
  return isMigItemApplied(migItem) || isMigItemAvailable(migItem);
}

/**
 *
 * @param migItem
 * @returns
 */
export function isMigItemTagged(migItem: MigItem): migItem is MigItemTagged {
  return [
    MigItemKind.T__,
    MigItemKind.T_A,
    MigItemKind.TM_,
    MigItemKind.TMA,
  ].includes(migItem.kind);
}

/**
 *
 * @param migItem
 * @returns
 */
export function isMigItemTaggedAvailable(
  migItem: MigItem
): migItem is MigItemTM_ | MigItemTMA {
  return isMigItemAvailable(migItem) && isMigItemTagged(migItem);
}

/**
 *
 * @param semVer
 * @returns
 */
export const isMigItemMatchingSemVer =
  (semVer: SemVer) =>
  (migItem: MigItem): boolean =>
    hasEqualSemVerPrecedence(migItem.semVer, semVer);

/**
 *
 * @param target
 * @returns
 */
export const isMigItemSatifyingTarget =
  (target: string) =>
  (migItem: MigItem): boolean =>
    isSemVerSatifyingTarget(migItem.semVer, target);

/**
 *
 * @param a
 * @param b
 * @returns
 */
export function orderForApplying(a: MigItem, b: MigItem): number {
  const sortAsc = sortAscSemVer(a.semVer, b.semVer);
  if (sortAsc !== 0) {
    return sortAsc;
  }

  return prioFromMigItems(a, b, "a");
}

/**
 *
 * @param a
 * @param b
 * @returns
 */
export function orderForReverting(a: MigItem, b: MigItem): number {
  const sortDesc = sortDescSemVer(a.semVer, b.semVer);
  if (sortDesc !== 0) {
    return sortDesc;
  }

  return prioFromMigItems(a, b, "b");
}

/**
 *
 * @param items
 * @param semVer
 * @returns
 */
export function migItemsFromSemVer(
  items: Array<MigrationTarget["val"]>,
  semVer: SemVer
): Array<MigrationTarget> {
  return items
    .map((val, idx) =>
      hasEqualSemVerPrecedence(semVer, val.semVer) ? { idx, val } : null
    )
    .filter((val): val is MigrationTarget => val !== null);
}

/**
 *
 * @param a
 * @param b
 * @param prio
 * @returns
 */
function prioFromMigItems(a: MigItem, b: MigItem, prio: "a" | "b"): number {
  const prioA = prio === "a" ? 1 : -1;
  const prioB = prioA === 1 ? -1 : 1;

  const isAppliedA = isMigItemApplied(a);
  const isAppliedB = isMigItemApplied(b);
  const isAvailableA = isMigItemAvailable(a);
  const isAvailableB = isMigItemAvailable(b);

  if (isAppliedA && isAppliedB && isAvailableA && isAvailableB) {
    // NOTE: should never occur
    return 0;
  }
  if (isAppliedA && isAppliedB && isAvailableA) {
    return prioA;
  }
  if (isAppliedA && isAppliedB && isAvailableB) {
    return prioB;
  }
  if (isAppliedA && isAppliedB) {
    // NOTE: should never occur
    return 0;
  }
  if (isAppliedA) {
    return prioA;
  }
  if (isAppliedB) {
    return prioB;
  }

  return 0;
}

/**
 *
 * @param a
 * @param b
 * @returns
 */
function isEqualMigration(
  a: { semVer: SemVer; sha256sum: Sha256Sum },
  b: { semVer: SemVer; sha256sum: Sha256Sum }
): boolean {
  return (
    a.sha256sum === b.sha256sum && hasEqualSemVerPrecedence(a.semVer, b.semVer)
  );
}
