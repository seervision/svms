export enum AddArgument {
  Major = "MAJOR", // bump a major version
  Minor = "MINOR", // bump a minor version
  Patch = "PATCH", // bump a patch version
  PreMajor = "PREMAJOR",
  PreMinor = "PREMINOR",
  PrePatch = "PREPATCH",
  PreRelease = "PRERELEASE",
}

export enum CheckoutArgument {
  Head = "HEAD", // latest available migration file
  Tag = "TAG", // latest release tag
}
