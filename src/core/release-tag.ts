import { Config, ReleaseTag } from "../resources";
import { SemVer, semVerParsed } from "./sem-ver";

/**
 *
 * TODO: Make this configurable via the config!
 *
 * @param config
 * @param releaseTag
 * @returns
 */
export function releaseTagOriginFromReleaseTag(
  config: Config,
  releaseTag: ReleaseTag
): string {
  return config.repository.tagPrefix
    ? config.repository.tagPrefix + "/" + releaseTag
    : releaseTag;
}

/**
 * Seervision doesn't stick quite well to the SemVer spec with its release tags, as the pre-release
 * part is used as substitute for the release name. This function is solely here to fix that.
 *
 * TODO: Make this configurable via the config!
 *
 * @param releaseTag
 */
export function semVerFromReleaseTag(releaseTag: ReleaseTag): SemVer | null {
  const [semVerUnparsed, ..._nameArr] = releaseTag.split("-");
  return semVerParsed(semVerUnparsed);
}
