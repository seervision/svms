import type {
  Config,
  ReleasePatch,
  ReleasesMajor,
  ReleasesMinor,
} from "../resources";
import type { Migration } from "./migration";

export type ScriptEnv = Record<string, string>;

/**
 *
 * @param config
 * @param migration
 */
export function scriptEnvFromMigration(
  config: Config,
  migration: Migration
): ScriptEnv {
  const envKey = "scriptEnv";

  const releaseBase = config.releases;
  const scriptEnvBase = releaseBase[envKey] || {};
  const majorStringified = migration.semVer.major.toString();
  if (!(majorStringified in releaseBase)) {
    return scriptEnvBase;
  }

  const releaseMajor = config.releases[majorStringified] as ReleasesMajor;
  const scriptEnvMajor = releaseMajor[envKey] || {};
  const minorStringified = migration.semVer.minor.toString();
  if (!(minorStringified in releaseMajor)) {
    return {
      ...scriptEnvBase,
      ...scriptEnvMajor,
    };
  }

  const releaseMinor = releaseMajor[minorStringified] as ReleasesMinor;
  const scriptEnvMinor = releaseMinor[envKey] || {};
  const patchStringified = migration.semVer.patch.toString();
  if (!(patchStringified in releaseMinor)) {
    return {
      ...scriptEnvBase,
      ...scriptEnvMajor,
      ...scriptEnvMinor,
    };
  }

  const releasePatch = releaseMinor[patchStringified] as ReleasePatch;
  const scriptEnvPatch = releasePatch[envKey] || {};
  return {
    ...scriptEnvBase,
    ...scriptEnvMajor,
    ...scriptEnvMinor,
    ...scriptEnvPatch,
  };
}
