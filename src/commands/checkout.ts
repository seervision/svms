import { Command, Flags } from "@oclif/core";

import * as core from "../core";
import { isMigrationAvailable } from "../core/migration";
import { isSemVerLte, isSemVerGte, isSemVerGt } from "../core/sem-ver";

enum Direction {
  Apply,
  Revert,
}

interface CheckoutTarget {
  direction: Direction;
  items: Array<core.MigItemApplied | core.MigItemMigration>;
}

export default class Checkout extends Command {
  static description = "Switch to a persistent data state for a release.";

  static examples = ["<%= config.bin %> <%= command.id %> ^1.0.0"];

  static flags = {
    dry: Flags.boolean({
      char: "d",
      default: false,
      description:
        "Make any changes as a dry-run, so no changes will be applied.",
    }),
    force: Flags.boolean({
      char: "f",
      default: false,
      description: "Force a checkout (use with care).",
    }),
    verbose: Flags.boolean({
      char: "v",
      default: false,
      description: "Verbose output to STDOUT.",
    }),
    fixConflicts: Flags.boolean({
      char: "c",
      default: false,
      description: "Resolve conflicts with already applied migrations.",
    }),
  };

  static args = [
    {
      name: "target",
      required: true,
      description: "semver target to checkout",
    },
  ];

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Checkout);
    const targetRaw = args.target;
    const dry = flags.dry;
    const force = flags.force;
    const verbose = flags.verbose;
    const fixConflicts = flags.fixConflicts;

    if (verbose) {
      this.log("Creating API...");
    }
    if (fixConflicts) {
      // Before doing anything else, check if there are migration items which
      // have a checksum mismatch. If true, find the one with the lower version
      // and check it out.
      const api = await core.createApi(this.config.configDir)
      await this.maybeFixMigrationConflicts(api, verbose, dry, force);
    }
    const api = await core.createApi(this.config.configDir);

    // checkout target
    const { direction, items } = this.checkoutTargetFromApi(
      targetRaw,
      api,
      verbose
    );

    // we might have nothing to do
    if (!items.length) {
      this.log("nothing to do");
      this.exit(0);
    }

    // ensure migration uniqueness
    const isAllUniqueCheck = items.reduce(
      (acc, val) =>
        !acc.result
          ? acc
          : {
              result: !acc.vals.some((prevVal) =>
                core.hasEqualSemVerPrecedence(prevVal.semVer, val.semVer)
              ),
              vals: [...acc.vals, val],
            },
      {
        result: true,
        vals: [] as core.MigItem[],
      }
    );
    if (!isAllUniqueCheck.result && !force) {
      this.error(
        "Some migrations have the same semver. The checksum of the migration script probably changed.\n" +
          "  (use -f to force checkout)\n",
        {
          exit: 1,
        }
      );
    }

    // ensure migration availability
    const isEveryMigrationAvailable = items.every(core.isMigItemAvailable);
    if (!isEveryMigrationAvailable && !force) {
      this.error(
        "Some migrations are missing for checkout.\n" +
          "  (use -f to force checkout)\n",
        {
          exit: 1,
        }
      );
    }

    // apply migrations
    switch (direction) {
      case Direction.Apply: {
        if (verbose) {
          this.log("Applying migrations...");
        }
        const itemsToApply = items.filter(core.isMigItemAvailable);
        for (const item of itemsToApply) {
          const migration = item.migration;
          if (verbose) {
            this.log(
              "  Applying migration: " + core.labelFromSemVer(item.semVer)
            );
          }
          if (!dry) {
            await api.apply(migration);
          }
        }
        break;
      }
      case Direction.Revert: {
        if (verbose) {
          this.log("Reverting migrations...");
        }
        const itemsToRevert = items.filter(core.isMigItemApplied);
        for (const item of itemsToRevert) {
          const migration = item.migration;
          if (verbose) {
            this.log(
              "  Reverting migration: " + core.labelFromSemVer(item.semVer)
            );
          }
          if (!dry) {
            await api.revert(migration);
          }
        }
        break;
      }
      default:
        ((val: never): never => {
          throw new Error(`Cannot migration into unknown direction: ${val}`);
        })(direction);
    }

    if (verbose) {
      this.log("");
    }
    this.log("done");

    this.exit();
  }

  /**
   *
   * @param targetRaw
   * @param api
   * @param verbose
   * @returns
   */
  private checkoutTargetFromApi(
    targetRaw: string,
    api: core.Api,
    verbose: boolean
  ): CheckoutTarget {
    //
    const items = api.items.filter(core.isMigItemMigration);
    if (!items.length) {
      return {
        direction: Direction.Revert,
        items,
      };
    }

    // find latest migration that satisfies the semver target
    const target = this.resolveTarget(targetRaw, api.items, verbose);
    if (!core.isValidSemVerTarget(target)) {
      this.error("Not a valid semver target: " + targetRaw, {
        exit: 1,
      });
    }

    // we might want to revert every migration
    const itemOldest = items[items.length - 1];
    if (
      core.isSemVerGtTarget(itemOldest.semVer, target) &&
      !core.isMigItemSatifyingTarget(target)(itemOldest)
    ) {
      return {
        direction: Direction.Revert,
        items: items.filter(core.isMigItemApplied),
      };
    }

    // we might want to apply every migration
    const itemNewest = items[0];
    if (
      core.isSemVerLtTarget(itemNewest.semVer, target) &&
      !core.isMigItemSatifyingTarget(target)(itemNewest)
    ) {
      return {
        direction: Direction.Apply,
        items: items.sort(core.orderForApplying),
      };
    }

    // find latest migration that satisfies the semver target
    if (verbose) {
      this.log("Resolving migration for target: " + target);
    }
    const migrationTarget = items.find(core.isMigItemSatifyingTarget(target));
    if (!migrationTarget) {
      this.error("No migration found that satisfies target: " + targetRaw, {
        exit: 1,
      });
    }
    const itemsTarget = core.migItemsFromSemVer(items, migrationTarget.semVer);
    if (!itemsTarget.length) {
      // this shouldn't be true, otherwise it would be an implementation error and we shouldn't continue
      this.error("Couldn't resolve any target resources", {
        exit: 1,
      });
    }

    // find latest applied migration
    if (verbose) {
      this.log("Resolving applied migration...");
    }
    const migrationApplied = items.find(core.isMigItemApplied);
    const itemsApplied = migrationApplied
      ? core.migItemsFromSemVer(items, migrationApplied.semVer)
      : [];

    // compute sliced items in order of execution
    //
    // NOTE: the way it's implemeted is by iterating through arrays. that way we can handle
    // conflicting semvers. conflicting semvers are resource items that have the same semver, but
    // differ in the checksum (sha256).
    // such a case could happen in a scenario like:
    // 1. migration script is created
    // 2. migration script is applied
    // 3. migration script is updated
    // ...now the checksum of the applied migration wouldn't match the checksum of the migration
    // script anymore, but both will have the same semver. to handle this conflict we need to pay
    // attention in the ordering of the items (depending on up-/downward migration) and should
    // always back that algorithm up with unit tests!
    const idxsTarget = itemsTarget.map(({ idx }) => idx);
    const idxsApplied = itemsApplied.map(({ idx }) => idx);
    const idxToMatch = Math.min(...idxsTarget, ...idxsApplied);
    const isReverting = idxsApplied.includes(idxToMatch); // NOTE: we default to going downwards to fix potential inconsistencies with duplicated semvers
    const idxTarget = isReverting
      ? Math.max(...idxsTarget)
      : Math.min(...idxsTarget);
    const idxApplied = !idxsApplied.length
      ? items.length
      : isReverting
      ? Math.min(...idxsApplied)
      : Math.max(...idxsApplied);
    const idxBegin = Math.min(idxTarget, idxApplied);
    const idxEnd = Math.max(idxTarget, idxApplied);
    const order = isReverting ? core.orderForReverting : core.orderForApplying;

    return {
      direction: isReverting ? Direction.Revert : Direction.Apply,
      items: items.slice(idxBegin, idxEnd).sort(order),
    };
  }

  /**
   * Resolve the semver target in label format.
   *
   * @param items
   * @param verbose
   * @param targetRaw
   * @returns
   */
  private resolveTarget(
    targetRaw: string,
    items: core.MigItem[],
    verbose: boolean
  ): string {
    switch (targetRaw) {
      case core.CheckoutArgument.Head: {
        if (verbose) {
          this.log(`Resolving ${core.CheckoutArgument.Head}...`);
        }
        const migItemAvailable = items.find(core.isMigItemAvailable);
        if (!migItemAvailable) {
          this.error("Cannot resolve latest migration file", { exit: 1 });
        }
        const label = core.labelFromSemVer(migItemAvailable.semVer);
        if (verbose) {
          this.log(`${core.CheckoutArgument.Head} is ${label}.`);
        }
        return label;
      }
      case core.CheckoutArgument.Tag: {
        if (verbose) {
          this.log(`Resolving ${core.CheckoutArgument.Tag}...`);
        }
        const migItemTaggedAvailable = items.find(
          core.isMigItemTaggedAvailable
        );
        if (!migItemTaggedAvailable) {
          this.error("Cannot resolve latest release tag", { exit: 1 });
        }
        const label = core.labelFromSemVer(migItemTaggedAvailable.semVer);
        if (verbose) {
          this.log(`${core.CheckoutArgument.Tag} is ${label}.`);
        }
        return label;
      }
      default:
        if (verbose) {
          this.log(`Using target as is (${targetRaw})...`);
        }
        return targetRaw;
    }
  }

  private async maybeFixMigrationConflicts(api: core.Api, verbose: boolean, dry: boolean, force: boolean): Promise<boolean> {
    if (verbose) {
      this.log('Checking for existing migration conflicts...');
    }
    const appliedMigItemsWithChecksumMismatch = this.findAppliedMigItemsWithChecksumMismatch(api.items, verbose);
    const availableUnappliedOlderMigItems = this.findAvailableUnappliedOlderMigItems(api.items, verbose);
    const migrationsWithIssues: core.MigItem[] = [...appliedMigItemsWithChecksumMismatch, ...availableUnappliedOlderMigItems];
    if (!migrationsWithIssues.length) {
      this.log('No fixable conflicts detected');
      return false;
    }
    const oldestMigrationWithIssue = migrationsWithIssues.reduce((eltA, eltB) => isSemVerLte(eltA.semVer, eltB.semVer) ? eltA : eltB);
    if (verbose) {
      this.log('The following versions have issues:');
      migrationsWithIssues.map((item) => this.log(`  ${core.labelFromSemVer(item.semVer)}`));
      this.log(`Reverting before version: ${core.labelFromSemVer(oldestMigrationWithIssue.semVer)}...`);
    }
    const availableMigrations = api.items.filter(core.isMigItemAvailable);
    const appliedMigrations = api.items.filter(core.isMigItemApplied);
    const itemsToRevert = appliedMigrations.filter((item) => isSemVerGte(item.semVer, oldestMigrationWithIssue.semVer)).sort(core.orderForReverting);
    for (const item of itemsToRevert) {
      const migration = item.migration;
      if (verbose) {
        this.log(
          `  Reverting migration: ${core.labelFromSemVer(item.semVer)}`
        );
      }
      let migrationToRevert = migration;
      if (!isMigrationAvailable(migration)) {
        const associatedAvaliableItem = availableMigrations.find((availableMigration) => core.hasEqualSemVerPrecedence(item.semVer, availableMigration.semVer))
        if (!!associatedAvaliableItem) {
          // The migration to be reverted must inherit the sha256sum from its applied counterpart
          // in order to be correctly unregistered from MongoDB.
          migrationToRevert = {...associatedAvaliableItem.migration,
                               ...migration}
        } else {
          if (!force) {
            this.error(
              `  Migration ${core.labelFromSemVer(item.semVer)} has no script. Cannot revert`
            );
          }
        }
      }
      if (!dry) {
        await api.revert(migrationToRevert);
      }
    }
    if (verbose) {
      this.log('Fixing of detected conflicts completed');
    }
    return true;
  }

  private findAppliedMigItemsWithChecksumMismatch(items: core.MigItem[], verbose: boolean): core.MigItemApplied[] {
    if (verbose) {
      this.log('Checking for applied migrations with script hash mismatch...');
    }
    const availableItems = items.filter(core.isMigItemAvailable);
    const appliedItems = items.filter(core.isMigItemApplied);
    const conflictingAppliedMigItems = appliedItems.filter((appliedItem) => {
      const availableItem = availableItems.find((availableItem) => core.hasEqualSemVerPrecedence(availableItem.semVer, appliedItem.semVer));
      if (!availableItem) {
        return false;
      }
      const hasConflict = appliedItem.migration.sha256sum !== availableItem.migration.sha256sum;
      if (hasConflict && verbose) {
        this.log(`Hash mismatch on ${core.labelFromSemVer(appliedItem.semVer)}: ${appliedItem.migration.sha256sum} vs ${availableItem.migration.sha256sum}`);
      }
      return hasConflict;
    })
    return conflictingAppliedMigItems;
  }

  private findAvailableUnappliedOlderMigItems(items: core.MigItem[], verbose: boolean): core.MigItemMigration[] {
    if (verbose) {
      this.log('Checking for not applied, available older migrations...');
    }
    const latestItemApplied = items.find(core.isMigItemApplied);
    if (!latestItemApplied) {
      return [];
    }
    const availableItems = items.filter(core.isMigItemAvailable);
    const appliedItems = items.filter(core.isMigItemApplied);
    const unappliedOlderMigItems = availableItems.filter((availableItem) => {
      if (isSemVerGt(availableItem.semVer, latestItemApplied.semVer)) {
        return false;
      }
      const appliedItem = appliedItems.find((appliedItem) => core.hasEqualSemVerPrecedence(availableItem.semVer, appliedItem.semVer));
      const isUnapplied = !appliedItem;
      if (isUnapplied && verbose) {
        this.log(`Migration script of version ${core.labelFromSemVer(availableItem.semVer)} is not applied`);
      }
      return isUnapplied;
    })
    return unappliedOlderMigItems;
  }
}
