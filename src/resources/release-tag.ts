import cp from "child_process";
import util from "util";

import { semVerFromReleaseTag } from "../core/release-tag";
import { SemVer, hasEqualSemVerPrecedence } from "../core/sem-ver";
import type { Config } from "../resources/config";

const exec = util.promisify(cp.exec);

/**
 * The part of the git tag that represents the SemVer and is used to determine the SemVer
 * precendence (major, minor, patch and pre-release).
 */
export type ReleaseTag = string;

/**
 * Get all available git release tags in arbitrary order.
 *
 * @param namespace
 * @returns
 */
export async function getReleaseTags(config: Config): Promise<ReleaseTag[]> {
  const cwd = config.repository.rootDir;
  const ns = config.repository.tagPrefix
    ? config.repository.tagPrefix + "/"
    : "";

  let tags = "";
  try {
    const { stdout } = await exec("git tag", {
      cwd,
      encoding: "utf8",
    });
    tags = stdout;
  } catch {
    // TODO: warn about missing git repository
    return [];
  }

  return tags
    .split("\n")
    .filter((val) => val && val.startsWith(ns))
    .map((val) => {
      const releaseTag = val.slice(ns.length);
      const semVer = semVerFromReleaseTag(releaseTag);

      return {
        releaseTag,
        semVer,
      };
    })
    .filter(
      (val): val is { releaseTag: ReleaseTag; semVer: SemVer } => !!val.semVer
    )
    .reduce<Array<{ releaseTag: ReleaseTag; semVer: SemVer }>>(
      (acc, val) =>
        acc.some((val2) => hasEqualSemVerPrecedence(val2.semVer, val.semVer))
          ? acc
          : [...acc, val],
      []
    )
    .map(({ releaseTag }) => releaseTag);
}
