# Architecture

This CLI tool lets you migrate a whole system in a semantic versioned way.

It syncs the following resources of a system:

- the git release tags of a repository
- migration scripts (currently supported are scripts written in `Python` and `Node.js`)
- a changelog that keeps track of which migrations are applied on the system
- the configuration for this CLI tool

...and then provides commands to act with those resources for a successful migration.

![concept.png](/docs/concept.png "Concept")

Resources are freshly gathered with every new invocation of a command. This is happening with the creation of a [`Api`](./src/core/api.ts) instance.
